const Cell = require('./Cell');
const { random, mathD } = require('../utils');
const Bullet = require('./Bullet');
const { uuid } = require('../utils');

class Player extends Cell {
  constructor(ws) {
    super(ws);
    this.socket = ws;
    this.server = require('../core/gameServer');
    this.bullets = [];
    this.speed = 0;
    this.deathHandler = null;
    this.color = '';
    this.radius = 25;
    this.health = 100;
    this.angle = 70;
    this.x = random(0, 10);
    this.y = random(0, 10);
  }
  update() {
    this.movement();
    console.log(`{ x : ${this.x} , y : ${this.y} }`);
  }
  death() {
    this.socket.packet.death();
    this.deathHandler(this.socket);
  }
  destroy() {
    this.update = () => {};
    this.movement = () => {};
  }
  set playerAngle(data) {
    this.angle = data;
    this.speed = 5;
  }
  get playerAngle() {
    return this.angle;
  }
  set setBullet(bullets) {
    this.bullets = [...bullets];
  }
  stop() {
    this.speed = 0;
  }
  movement() {
    const tempX = this.x + (this.speed * mathD.cos(this.angle));
    const tempY = this.y + (this.speed * mathD.sin(this.angle));
    if ((tempX >= 0 && tempX <= this.server.universe.x) ||
        (tempX < 0 && (tempX >= (this.server.universe.x * -1)))) {
      this.x = tempX;
    }
    if ((tempY >= 0 && tempY <= this.server.universe.y) ||
        (tempY < 0 && (tempY >= (this.server.universe.y * -1)))) {
      this.y = tempY;
    }
  }
  shoot() {
    const tempBullet = new Bullet(this.x, this.y);
    tempBullet.setIndex = this.bullets.length;
    tempBullet.setUid = uuid();
    tempBullet.setAngle = this.angle;
    this.socket.packet.playerShooted(this.socket.uid);
    this.bullets.push(tempBullet);
  }
  getHurted(bullet) {
    console.log('geted hurt');
    bullet.finish = true;
    this.health -= bullet.damage;
    if (this.health <= 0) {
      this.death();
    }
  }
  getEnemies() {
    return this.server.clients
      .filter(client => client.uid !== this.socket.uid)
      .map(client => ({
        ...client.player.getState(),
        ...client.player.getPosition(),
        uid: client.uid,
      }));
  }
  getPosition() {
    return {
      x: this.x,
      y: this.y,
    };
  }
  getState() {
    return {
      angle: this.angle,
      health: this.health,
    };
  }
  getColor() {
    return this.color;
  }
  setColor(data) {
    this.color = data;
  }
  collision(bullet) {
    const tempR = Math.sqrt(((this.x - bullet.x) ** 2) + ((this.y - bullet.y) ** 2));
    if (tempR <= this.radius) {
      this.getHurted(bullet);
      return true;
    }
    return false;
  }
}
module.exports = Player;
