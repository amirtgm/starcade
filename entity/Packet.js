

// const server = new GameServer();

class Packet {
  constructor(ws) {
    this.socket = ws;
    this.server = require('../core/gameServer');
  }
  listen() {
    this.socket.on('message', (res) => {
      const response = JSON.parse(res);
      switch (response.type) {
        case 'stop':
          this.socket.player.stop();
          break;
        case 'angle':
          this.socket.player.playerAngle = parseInt(response.data, 10);
          break;
        case 'fire':
          this.socket.player.shoot();
          break;
        case 'color':
          this.socket.player.setColor(response.data);
          break;
        default:
          break;
      }
    });
  }
  destroy() {
    this.sendPosition = () => {};
    this.listen = () => {};
  }
  death() {
    this.server.clients.map((client) => {
      client.send(JSON.stringify({ type: 'death', uid: this.socket.uid }));
    });
  }
  sendPosition() {
    const data = {};
    data.type = 'position';
    data.mySelf = {
      ...this.socket.player.getState(),
      ...this.socket.player.getPosition(),
      uid: this.socket.uid,
    };
    data.enemies = this.socket.player.getEnemies();
    this.socket.send(JSON.stringify(data));
  }
  connectionClose(uid) {
    this.server.clients.map((client) => {
      client.send(JSON.stringify({ type: 'connectionClose', uid }));
    });
  }
  playerShooted(uid) {
    this.server.clients.map((client) => {
      if (client.uid !== uid) {
        client.send(JSON.stringify({ type: 'fire', uid }));
      }
    });
  }
  newPlayer() {
    this.server.clients.map((client) => {
      if (client.uid !== this.socket.uid) {
        client.send(JSON.stringify({ type: 'newEnemy', uid: this.socket.uid, color: client.color }));
      }
    });
  }
}
module.exports = Packet;
