// const Vector = require('../entity').Vector;

class Cell {
  constructor(nodeId, owner, position, gameServer) {
    this.nodeId = nodeId;
    this.owner = owner; // playerTracker that owns this cell

    // this.position = null;
    // if (position) {
    //   this.position = new Vector(position.x, position.y);
    // } else {
    //   this.position = new Vector(0, 0);
    // } // needs to change to spawn randomly
    this.cellType = -1; // 0 = Player Cell, 1 = bullet

    this.killedBy = null; // Cell that killed this cell
    this.gameServer = gameServer;
    this.speed = null;
  //  this.moveEngine = new Vector();
  }
  // getSize() {}
  move() { // movment of the cell based on this.speed
    return this.speed;
  }
}
module.exports = Cell;
