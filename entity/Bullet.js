const Cell = require('./Cell');
const { mathD } = require('../utils');

class Bullet extends Cell {
  constructor(x, y) {
    super();
    this.startPoint = {
      x,
      y,
    };
    this.x = x;
    this.y = y;
    this.shootedBy = null;
    this.finish = false;
    this.uid = null;
    this.index = null;
    this.type = 'laser';
    this.angle = null;
    this.speed = 7;
    this.range = 150;
    this.damage = 50;
  }
  set setIndex(index) {
    this.index = index;
  }
  get setIndex() {
    return this.index;
  }
  set setUid(uid) {
    this.uid = uid;
  }
  get setUid() {
    return this.uid;
  }
  set setAngle(angle) {
    this.angle = angle;
  }
  get setAngle() {
    return this.angle;
  }
  movement() {
    const tempX = this.x + (this.speed * mathD.cos(this.angle));
    const tempY = this.y + (this.speed * mathD.sin(this.angle));
    const tempR = Math.sqrt(((tempX - this.startPoint.x) ** 2) + ((tempY - this.startPoint.y) ** 2));
    if (tempR <= this.range) {
      console.log('tempR', tempR);
      this.x = tempX;
      this.y = tempY;
    } else {
      this.finish = true;
    }
  }
}

module.exports = Bullet;
