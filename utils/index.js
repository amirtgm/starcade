const random = require('./Random');
const mathD = require('./MathD');
const uuid = require('./uuid');

module.exports = {
  random,
  uuid,
  mathD,
};
