// helper
/**
 * converts degree to radians
 * @param degree
 * @returns {number}
 */
const toRadians = degree => degree * (Math.PI / 180);

/**
 * Converts radian to degree
 * @param radians
 * @returns {number}
 */
const toDegree = radians => radians * (180 / Math.PI);

/**
 * Rounds a number mathematical correct to the number of decimals
 * @param number
 * @param decimals (optional, default: 5)
 * @returns {number}
 */
const roundNumber = (number, decimals) => {
  decimals = decimals || 5;
  return Math.round(number * (10 ** decimals)) / (10 ** decimals);
};
// the object
const MathD = {
  sin(number) {
    return roundNumber(Math.sin(toRadians(number)));
  },
  cos(number) {
    return roundNumber(Math.cos(toRadians(number)));
  },
  tan(number) {
    return roundNumber(Math.tan(toRadians(number)));
  },
  asin(number) {
    return roundNumber(toDegree(Math.asin(number)));
  },
  acos(number) {
    return roundNumber(toDegree(Math.acos(number)));
  },
  atan(number) {
    return roundNumber(toDegree(Math.atan(number)));
  },
};
module.exports = MathD;

