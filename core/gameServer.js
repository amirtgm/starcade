const WebSocket = require('ws');
const Loop = require('./loop');
const { Player, Packet } = require('../entity');
const { uuid } = require('../utils');

let instance = null;
class GameServer {
  constructor(port) {
    if (!instance) { // singleton structure
      this.clients = [];
      this.port = port;
      this.universe = {
        x: 5000,
        y: 5000,
      };
      instance = this;
    }
    return instance;
  }
  start() {
    // Start the server
    this.loop = new Loop(this);
    this.socketServer = new WebSocket.Server({
      port: this.port,
      perMessageDeflate: false,
    }, () => {
      this.loop.start();
    });

    const connectionClose = (ws) => {
      ws.player.destroy();
      ws.packet.destroy();
      let clientIndex = null;
      this.clients.map((client, index) => {
        if (client.uid === ws.uid) {
          clientIndex = index;
          console.log('a player just disconnected with the uid of :', ws.uid);
        }
        return client;
      });
      if (typeof clientIndex === 'number') {
        this.clients.splice(clientIndex, 1);
      }
      this.clients.map((client) => {
        client.packet.connectionClose(ws.uid);
      });
    };
    const connectionEstablished = (ws) => {
      ws.remoteAddress = ws._socket.remoteAddress;
      ws.remotePort = ws._socket.remotePort;
      ws.uid = uuid();
      console.log('a player just connected with the uid of :', ws.uid);
      ws.player = new Player(ws);
      ws.packet = new Packet(ws);
      ws.player.packetHandler = ws.packet;
      ws.player.deathHandler = connectionClose;
      this.clients.push(ws);
      ws.packet.newPlayer();
      ws.packet.listen();
      ws.on('error', () => connectionClose(ws));
      ws.on('close', () => connectionClose(ws));
      // console.log(ws);
      ws.send(JSON.stringify({ uid: ws.uid }));
    };
    this.socketServer.on('connection', ws => connectionEstablished(ws));

    // Properly handle errors because some people are too lazy to read the readme
    this
      .socketServer
      .on('error', (e) => {
        switch (e.code) {
          case 'EADDRINUSE':
            console.log("[Error] Server could not bind to port! Please close out of Skype or change 'serv" +
                                "erPort' in gameserver.ini to a different number.");
            break;
          case 'EACCES':
            console.log('[Error] Please make sure you are running Ogar with root privileges.');
            break;
          default:
            console.log(`[Error] Unhandled error code: ${e.code}`);
            break;
        }
        process.exit(1); // Exits the program
      });
  }
}
module.exports = new GameServer(process.env.PORT);
