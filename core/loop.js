class loop {
  constructor(server) {
    this.time = new Date();
    this.startTime = this.time;
    this.internalClock = 0;
    this.lastUpdate = -40;
    this.ticks = 40;
    this.tickLB = 0; // 40 ms ticks, 4 - update leaderboard
    this.updateLog = {};
    this.gameServer = server;
  }

  start() {
    this.updateLoop();
  }
  updateLoop() {
    this.update();
    this.queueUpdate(this.ticks);
  }
  queueUpdate(ticks) {
    setTimeout(() => this.updateLoop(), ticks);
  }
  update() {
    this.gameServer.clients.map((client) => {
      client.player.movement();
      if (client.player.bullets.length > 0) {
        client.player.setBullet = client.player.bullets.map((bullet) => {
          if (!bullet.finish) {
            bullet.movement();
            this.gameServer.clients.map((cl) => {
              if (client.uid !== cl.uid) {
                cl.player.collision(bullet);
              }
            });
            return bullet;
          }
        }).filter(x => x);
      }
    });
    this.gameServer.clients.map((client) => {
      client.packet.sendPosition();
    });
  }
}
module.exports = loop;
