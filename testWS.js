const WebSocket = require('ws');

const ws = new WebSocket('ws://localhost:3000');
let flag = true;
ws.onopen = (data, msg) => {
  console.log('open');
  console.log(data, msg);
  const Obj = {
    type: 'angle',
    data: 130,
  };
  ws.send(JSON.stringify(Obj));
};

ws.onmessage = (ev) => {
  const _data = JSON.parse(ev.data);
  console.log(_data);
  if (flag) {
    ws.send(JSON.stringify({ type: 'fire' }));
    ws.send(JSON.stringify({ type: 'stop' }));
    flag = false;
  }
};
