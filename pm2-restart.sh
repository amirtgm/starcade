if [[ $1 == *l* ]]
then
  git pull
fi

if [[ $1 == *i* ]]
then
  npm install
fi

pm2 restart ecosystem.json